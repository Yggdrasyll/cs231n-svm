#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 09:45:08 2018

@author: Ismael Chouraqi
"""

import numpy as np
import matplotlib.pyplot as plt

import os

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' + directory)

class Hingeloss:
    @staticmethod
    def Forward(scores, labels):
        """
        labels : vecteur de longueur L
        scores : matrice de taille LxM
        """
        Nb_train = scores.shape[1]
        true_scores = scores[labels, np.arange(scores.shape[1])]
        margins = np.maximum(scores - true_scores + 1, 0)
        margins[labels, np.arange(Nb_train)] = 0
        return np.sum(margins)*1./Nb_train # Loss
    
    @staticmethod
    def Backward(dLoss, scores, labels):       
        Nb_train = scores.shape[1]
        true_scores = scores[labels, np.arange(scores.shape[1])]
        dLoss /= 1.*Nb_train
        dmargins = scores - true_scores + 1
        dmargins[dmargins < 0 ] = 0
        dmargins[dmargins > 0 ] = dLoss
        dmargins[labels, np.arange(scores.shape[1])] = 0
        dmargins[labels, np.arange(scores.shape[1])] = -np.sum(dmargins, axis = 0)
        return dmargins
    
class Input:
    @staticmethod
    def Forward(W, x):
        return np.dot(W, x)
    
    @staticmethod
    def Backward(dmargin, x): 
        return np.dot(dmargin, x.T) # dW
        
class Regularisation:
    @staticmethod
    def Forward(W, Lambda):
        return Lambda * np.sum(W*W) * 0.5
    
    @staticmethod
    def Backward(W, Lambda):
        return Lambda * W
 
if __name__ == "__main__" :
    Datas = np.array([[0.5, 0.4], [0.8, 0.3], [0.3, 0.8], [-0.4, 0.3], [-0.3, 0.7], [-0.7, 0.2], [0.7, -0.4], [0.5, -0.6], [-0.4, -0.5]]).T
    y = np.array(np.concatenate((np.zeros([3, 1]), np.ones([3, 1]), 2 * np.ones([3, 1])) , axis = 0))
    labels = np.array([int(y[i]) for i in range(9)])[np.newaxis, :]
    
    Datas = np.concatenate([Datas, np.ones([1,Datas.shape[1]])], 0) # pour le biais
    nombre_label = np.max(labels)
    W = np.random.randn(nombre_label+1, Datas.shape[0]) / np.sqrt(Datas.shape[0]*1./2)
    Lossevo = []
    x = np.arange(-1,1,0.1)
    createFolder('Graph_for_each_iteration/')
    for itera in range(300):
        # plot
        plt.plot(Datas[0, :3], Datas[1, :3], 'bo')
        plt.plot(Datas[0, 3:6], Datas[1, 3:6], 'ro')
        plt.plot(Datas[0,6 : 9], Datas[1, 6:9], 'go')
        # Forward
        scores = Input.Forward(W, Datas)
        Loss = Hingeloss.Forward(scores, labels) + Regularisation.Forward(W, 0.4)
        Lossevo.append(Loss)
        
        
        # Backward
        dmargins = Hingeloss.Backward(1, scores, labels)
        dW = Input.Backward(dmargins, Datas) +  Regularisation.Backward(W, 0.4)
        
        # Update
        learning_rate = 0.05
        W -= learning_rate * dW
        if itera % 20 == 0 :
            print ("Loss after {} iteration(s) = {},  with a learning rate set to {}".format(itera, Loss, learning_rate))
        
        
        # Grad plot
    
        plt.plot(x, -W[0,0]/W[0, 1]*x - W[0, 2], 'b' )
        plt.quiver(0, -W[0, 2], [W[0,0]], [W[0,1] ], color = 'b')
        plt.plot(x, -W[1,0]/W[1, 1]*x - W[1, 2] , 'r' )
        plt.quiver(0, -W[1, 2], [W[1,0]], [W[1,1]], color = 'r')
        plt.plot(x, -W[2,0]/W[2, 1]*x - W[2, 2], 'g' )
        plt.quiver(0, -W[2, 2], [W[2,0]], [W[2,1]], color = 'g')
        plt.xlim([-1, 1])
        plt.ylim([-1, 1])
        plt.savefig("Graph_for_each_iteration/%d.png" % (itera))   
        plt.close('all')
        
        
    
    
