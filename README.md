# CS231n SVM implementation

I made this script for pedagogical purposes, and also to train. I tried to write it with the concern to be understandable from the pedagogical point of view, as well for the beginners in Python as for the beginners in machine learning. This Python script would be my solution for the CS231n assignment provided by Andrej Karpathy. If you use this code or if this code was useful to you, do not forget to mention it in your contributions.

## Getting Started


### Prerequisites

* Numpy
* Matplotlib.pyplot
* Os

For Windows users, you will need to change '/' by '\\'.

### Running the tests

Run the NN_POO.py script in a terminal
```
python NN_POO.py
```

## Built With

* [Python](https://www.python.org/downloads/) 


## Authors

* **Ismaël Chouraqi** - *Ph.D. student at Université Paris Descartes, MAP5* - [Gitlab](https://gitlab.com/Yggdrasyll)


## Acknowledgments

* Andrej Karpathy Winter Class 2016
* [HTML code](http://vision.stanford.edu/teaching/cs231n-demos/linear-classify/) by Andrej Karpathy.



